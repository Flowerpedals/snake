# snake

# travis

[![Build Status](https://travis-ci.com/Flowerpedals/snake.svg)](https://travis-ci.com/Flowerpedals/snake)

# CodeCov

[![codecov](https://codecov.io/gh/Flowerpedals/snake/branch/develop/graph/badge.svg)](https://codecov.io/gh/Flowerpedals/snake)

## Shields

[![docker build](https://img.shields.io/docker/build/lacribeiro11/snake)](https://img.shields.io/docker/build/lacribeiro11/snake)

## General Information

- Refactored by Group 1: Bajic, Eibensteiner, Ribeiro, Svoboda and Wolfram-Zak
- Language: Java JDK 11 - when you have more versions of Java it is important to make sure that the correct version will
  be used Windows & Mac: ensure this is to set the $JAVA_HOME Environment Variable to JDK 11
  Linux: https://askubuntu.com/questions/121654/how-to-set-default-java-version
- Maven Version 3.6.3 ! <3
- Git Repo: https://gitlab.com/Flowerpedals/snake.git
- IDE: IntelliJ Idea

## after repository checkout:
`mvn clean compile`

## to build a jar file:
`mvn clean verify`

## running the Project:
`mvn javafx:run`

