FROM openjdk

COPY ./target/exercise3-0.0.1-SNAPSHOT.jar /snake.jar

ENTRYPOINT ["/usr/bin/java", "-jar", "/snake.jar"]
