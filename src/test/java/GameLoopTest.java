import at.fhcampus.asd.exercise3.sample.GameLoop;
import javafx.stage.Stage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.testfx.api.FxAssert;
import org.testfx.framework.junit5.ApplicationTest;

public class GameLoopTest extends ApplicationTest {

    private Stage stage;

    @Override
    public void start(Stage stage) throws Exception {
        GameLoop gameLoop = new GameLoop();
        gameLoop.start(stage);
        this.stage = stage;
    }

    @Test
    public void checkWindowTitle_Equals_RainbowSnake() {
        System.out.println("Just a Test!");
        Assertions.assertEquals(stage.getTitle() , "Rainbow Snake");
        stage.getScene().getHeight();
    }

    @Test
    public void checkWindowHeightWidth() {
        Assertions.assertEquals(1500.0, stage.getScene().getWidth());
        Assertions.assertEquals(700.0, stage.getScene().getHeight());
    }

    @Test
    public void checkIfMainSceneIsShown() throws InterruptedException {
        Thread.sleep(10000);
        Assertions.assertEquals("0x006400ff", stage.getScene().getFill().toString());
        Assertions.assertEquals(1, stage.getScene().getRoot().getChildrenUnmodifiable().size());
    }





}
