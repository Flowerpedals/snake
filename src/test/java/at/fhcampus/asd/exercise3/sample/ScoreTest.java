package at.fhcampus.asd.exercise3.sample;

import javafx.stage.Stage;
import org.junit.jupiter.api.Test;
import org.testfx.framework.junit5.ApplicationTest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ScoreTest extends ApplicationTest {

    @Override
    public void start(Stage stage) throws Exception {
        GameLoop gameLoop = new GameLoop();
        gameLoop.start(stage);
    }

    @Test
    void scoreRespawn() {
        Score score = new Score(GameLoop.root);
        assertEquals(0, score.getScoreCard());
    }

    @Test
    void upScoreValue() {
        Score score = new Score(GameLoop.root);
        score.upScoreValue();
        score.upScoreValue();
        int test = 0;
        assertTrue(score.getScoreCard() > test,"1 is greater then 0");
    }

}