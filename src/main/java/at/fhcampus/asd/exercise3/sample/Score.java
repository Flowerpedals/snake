package at.fhcampus.asd.exercise3.sample;

import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.text.Font;

public class Score {

    private int scoreValue = 0;
    private static final String TEXTSCORE = "Score: ";
    Label scoreCard = new Label(TEXTSCORE + scoreValue);

    public Score(Group group) {
        scoreRespawn(group);
    }


    public void scoreRespawn(Group group) {
        scoreValue = 0;
        scoreCard.setFont(new Font("Arial", 50));
        setTextImpl();
        group.getChildren().add(scoreCard);
    }

    public void upScoreValue() {
        scoreValue++;
        setTextImpl();
    }

    private void setTextImpl() {
        scoreCard.setText(TEXTSCORE + scoreValue);
    }

    public int getScoreCard() {
        return scoreValue;
    }

}
