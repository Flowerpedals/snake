package at.fhcampus.asd.exercise3.sample;


import at.fhcampus.asd.exercise3.sample.messages.TouchMessageHelper;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

//Labels
public class Gameboard {

    public void setDeathTouchWall(Score score, Group group, Stage stage) {
        TouchMessageHelper touchMessageHelper = new TouchMessageHelper();
        Label deathTouchWall = new Label(touchMessageHelper.getRandomTouchWallMessage().getMessage()
                + "\nPress R for respawn" + "\nScore: " + score.getScoreCard());
        deathTouchWall.setFont(new Font("Calibri",80));
        deathTouchWall.setTextFill(Color.BLACK);

        group.getChildren().clear();
        deathTouchWall.relocate(200, stage.getHeight()/2-300);
        group.getChildren().add(deathTouchWall);

    }

    public void setDeathTouchTail(Score score, Group group, Stage stage) {
        TouchMessageHelper touchMessageHelper = new TouchMessageHelper();
        Label deathTouchTail = new Label(touchMessageHelper.getRandomTouchTailMessage().getMessage() + "\nPress R for respawn"
                +"\nScore: " + score.getScoreCard());
        deathTouchTail.setFont(new Font("Calibri",80));
        deathTouchTail.setTextFill(Color.BLACK);

        group.getChildren().clear();
        deathTouchTail.relocate(200, stage.getHeight()/2-200);
        group.getChildren().add(deathTouchTail);

    }

}
