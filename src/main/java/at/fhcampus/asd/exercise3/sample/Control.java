package at.fhcampus.asd.exercise3.sample;

import javafx.scene.Group;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class Control {

    private boolean goUp;
    private boolean goDown;
    private boolean goRight;
    private boolean goLeft;

    public void stopMovement() {
        goUp = false;
        goDown = false;
        goRight = false;
        goLeft = false;
    }


    public boolean getgoUp() {
        return goUp;
    }

    public boolean getgoDown() {
        return goDown;
    }

    public boolean getgoLeft() {
        return goLeft;
    }

    public boolean getgoRight() {
        return goRight;
    }


    public void keyHandler(KeyEvent keyEvent, Snake snake, Group group, GameObject food, Score score, Stage stage) {

        switch (keyEvent.getCode()) {
            case W:
                // you cannot move down and up at the same time
                if (goDown)
                    break;
                goUp = true;
                // set other directions to false to avoid diagonal movement
                goDown = false;
                goRight = false;
                goLeft = false;
                break;

            case S:
                if (goUp)
                    break;
                goDown = true;
                goUp = false;
                goRight = false;
                goLeft = false;
                break;

            case D:
                if (goLeft)
                    break;
                goRight = true;
                goUp = false;
                goDown = false;
                goLeft = false;
                break;
            case A:
                if (goRight)
                    break;
                goLeft = true;
                goUp = false;
                goDown = false;
                goRight = false;
                break;
            case R:
                snake.respawn(group, food, score, stage, this);
                GameLoop.stopGameovermusic();
                GameLoop.restartIngamemusic();
                break;
            default:
                break;
        }
    }
}

