package at.fhcampus.asd.exercise3.sample.messages;

import at.fhcampus.asd.exercise3.sample.enums.TouchTailMessages;
import at.fhcampus.asd.exercise3.sample.enums.TouchWallMessages;

import java.util.ArrayList;
import java.util.Random;

public class TouchMessageHelper {

    private ArrayList<TouchTailMessages> touchTailMessages;

    private ArrayList<TouchWallMessages> touchWallMessages;

    public TouchMessageHelper() {
        this.touchTailMessages = new ArrayList<>();
        touchTailMessages.add(TouchTailMessages.TOUCHTAIL1);
        touchTailMessages.add(TouchTailMessages.TOUCHTAIL2);
        touchTailMessages.add(TouchTailMessages.TOUCHTAIL3);
        touchTailMessages.add(TouchTailMessages.TOUCHTAIL4);
        touchTailMessages.add(TouchTailMessages.TOUCHTAIL5);
        touchTailMessages.add(TouchTailMessages.TOUCHTAIL6);

        this.touchWallMessages = new ArrayList<>();
        touchWallMessages.add(TouchWallMessages.TOUCHWALL1);
        touchWallMessages.add(TouchWallMessages.TOUCHWALL2);
        touchWallMessages.add(TouchWallMessages.TOUCHWALL3);
        touchWallMessages.add(TouchWallMessages.TOUCHWALL4);
        touchWallMessages.add(TouchWallMessages.TOUCHWALL5);
        touchWallMessages.add(TouchWallMessages.TOUCHWALL6);
        touchWallMessages.add(TouchWallMessages.TOUCHWALL7);
        touchWallMessages.add(TouchWallMessages.TOUCHWALL8);
        touchWallMessages.add(TouchWallMessages.TOUCHWALL9);
    }

    public ArrayList<TouchTailMessages> getTouchTailMessages() {
        return touchTailMessages;
    }

    public ArrayList<TouchWallMessages> getTouchWallMessages() {
        return touchWallMessages;
    }

    public  TouchTailMessages getRandomTouchTailMessage() {
        ArrayList<TouchTailMessages> touchTailMessages = getTouchTailMessages();
        Random rand = new Random();
        return touchTailMessages.get(rand.nextInt(touchTailMessages.size()));
    }

    public TouchWallMessages getRandomTouchWallMessage() {
        ArrayList<TouchWallMessages> touchWallMessages = getTouchWallMessages();
        Random rand = new Random();
        return touchWallMessages.get(rand.nextInt(touchWallMessages.size()));
    }

}
