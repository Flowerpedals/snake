package at.fhcampus.asd.exercise3.sample.enums;

public enum TouchTailMessages {
    TOUCHTAIL1("Touching yourself huh? ; )"),
    TOUCHTAIL2( "Snake ate herself in fury"),
    TOUCHTAIL3("Not your best day is it?...."),
    TOUCHTAIL4("Well...you tried..."),
    TOUCHTAIL5("Stop trying..."),
    TOUCHTAIL6("You touched that ass (tail..)!");

    private final String message;

    private TouchTailMessages(String s) {
        message = s;
    }

    public String getMessage() {
        return message;
    }

}
