package at.fhcampus.asd.exercise3.sample.enums;

public enum TouchWallMessages {
    TOUCHWALL1( "Stop touching the wall \nas if it's your boyfriend....."),
    TOUCHWALL2( "You never touch me \nin the way you touched that wall :*("),
    TOUCHWALL3( "Walls are your favorite thing huh?"),
    TOUCHWALL4( "The wall you touched is solid,\n no comin through"),
    TOUCHWALL5( "Wall:1, You:0"),
    TOUCHWALL6("Mimimimi you dead!"),
    TOUCHWALL7("No one would survive this..."),
    TOUCHWALL8("You colored the wall, \nwhat a nice thing to do"),
    TOUCHWALL9("No touchy touchy le wall mi Friend! ");

    private final String message;

    private TouchWallMessages( String s) {
        message = s;
    }

    public String getMessage() {
        return this.message;
    }


}
