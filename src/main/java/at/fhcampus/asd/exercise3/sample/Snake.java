package at.fhcampus.asd.exercise3.sample;

import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.util.LinkedList;

public class Snake {
    public static final long STANDARDFRAMEDELAY = 25000000;

    private long frameDelay = STANDARDFRAMEDELAY;
    private Rectangle head = new Rectangle(20, 20);
    private LinkedList<Rectangle> snakeRectangleList = new LinkedList<>();

    public Snake(Group group, Stage stage) {
        snakeRectangleList.add(head);
        snakeRectangleList.getFirst().relocate(stage.getWidth() / 2, stage.getHeight() / 2);
        group.getChildren().add(snakeRectangleList.getFirst());
    }

    public long getframeDelay() {
        return frameDelay;
    }


    public void respawn(Group group, GameObject food, Score score, Stage stage, Control control) {
        group.getChildren().clear();
        snakeRectangleList.clear();

        snakeRectangleList.add(head);
        snakeRectangleList.getFirst().relocate(stage.getWidth() / 2, stage.getHeight() / 2);
        group.getChildren().add(snakeRectangleList.getFirst());
        food.setFood(group, stage);
        score.scoreRespawn(group);
        frameDelay = STANDARDFRAMEDELAY;

        control.stopMovement();
    }

    public void snakeDead(Group group, Control control, Stage stage) {
        group.getChildren().clear();
        snakeRectangleList.clear();
        snakeRectangleList.add(head);
        snakeRectangleList.getFirst().relocate(stage.getWidth() / 2, stage.getHeight() / 2);
        frameDelay = STANDARDFRAMEDELAY;
        control.stopMovement();
    }


    public void eat(Group group, Score score, GameObject food) {
        snakeRectangleList.add(new Rectangle(20, 20));
        snakeRectangleList.getLast().setFill(Color.color(food.getColor()[0], food.getColor()[1], food.getColor()[2]));
        group.getChildren().add(snakeRectangleList.getLast());
        score.upScoreValue();
        long maxFrameDelay = 8000000;
        if (frameDelay >= maxFrameDelay) {
            long delayDecrease = 600000;
            frameDelay -= delayDecrease;
            System.out.println(frameDelay);
        }

    }

    public void collision(GameObject food, Group group, Bounds foodBound, Score score, Control control, Stage stage, Gameboard gameboard) { //gameobject sind obstacles so wie Food, Boundarys für Collisions
        Bounds headBox = head.getBoundsInParent();

        if (headBox.intersects(foodBound)) {
            eat(group, score, food);
            food.setFood(group, stage);
            GameLoop.playEatsound();
        }

        // Check if snake ran into the wall
        if (head.getLayoutX() <= 0 || head.getLayoutX() >= stage.getWidth() - 30 ||
                head.getLayoutY() <= 0 || head.getLayoutY() >= stage.getHeight() - 54) {
            snakeDead(group, control, stage);
            gameboard.setDeathTouchWall(score, group, stage);
            GameLoop.playDeathsound();
            GameLoop.stopIngamemusic();
            GameLoop.restartGameovermusic();
        }

        // Check if snake ran into itself
        for (int i = 1; i < this.snakeRectangleList.size(); i++) {
            if (headBox.intersects(this.snakeRectangleList.get(i).getBoundsInParent())) {
                System.out.println("DEAD");
                snakeDead(group, control, stage);
                gameboard.setDeathTouchTail(score, group, stage);
                GameLoop.playDeathsound();
                GameLoop.stopIngamemusic();
                GameLoop.restartGameovermusic();
            }
        }
    }


    public void moveSnake(int dx, int dy) {
        if (dx != 0 || dy != 0) {
            LinkedList<Rectangle> snakehelp = new LinkedList<>();

            for (int i = 0; i < snakeRectangleList.size(); i++) {
                snakehelp.add(new Rectangle());
                snakehelp.get(i).relocate(snakeRectangleList.get(i).getLayoutX(), snakeRectangleList.get(i).getLayoutY());
            }

            int x = (int) snakeRectangleList.getFirst().getLayoutX() + dx;
            int y = (int) snakeRectangleList.getFirst().getLayoutY() + dy;
            snakeRectangleList.getFirst().relocate(x, y);

            for (int i = 1; i < snakeRectangleList.size(); i++) {
                int helpX = (int) snakehelp.get(i - 1).getLayoutX();
                int helpY = (int) snakehelp.get(i - 1).getLayoutY();
                snakeRectangleList.get(i).relocate(helpX, helpY);
            }
        }
    }
}
