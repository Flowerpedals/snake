package at.fhcampus.asd.exercise3.sample;

import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;


public class GameObject {

    private static final double HEIGHT = 20;
    private static final double WIDTH = 20;
    private static final int DISTANCE = 50;

    //public to receive X/Y coordinates
    public static final Rectangle FOOD = new Rectangle(WIDTH, HEIGHT);
    private double redPart;
    private double greenPart;
    private double bluePart;
    private Bounds boundaries;

    public double[] getColor() {
        // returns a double Array with the colors of the tail of the snake
        // is called after eat
        double[] colors = new double[3];
        colors[0] = redPart;
        colors[1] = greenPart;
        colors[2] = bluePart;
        return colors;
    }

    public void setFood(Group g, Stage stage) {
        // to remove previous food
        g.getChildren().remove(FOOD);
        Random random = null;
        try {
            random = SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException e) {
            // This will never happen
        }

        assert random != null;
        redPart = random.nextDouble();
        greenPart = random.nextDouble();
        bluePart = random.nextDouble();
        // setting random colors for the food ( and later tail)
        FOOD.setFill(Color.color(redPart, greenPart, bluePart));
        // Random Location with a distance from the boundaries of DISTANCE
        FOOD.relocate(random.nextInt((int) stage.getWidth() - DISTANCE), random.nextInt((int) stage.getHeight() - DISTANCE));
        g.getChildren().add(FOOD);
        boundaries = FOOD.getBoundsInParent();
    }

    public Bounds getBound() {
        return boundaries;
    }
}
